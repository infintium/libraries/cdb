package cdb

import (
	"fmt"
	"go/types"
	"math"
	"sort"
	"strconv"
	"strings"
)

type ByteFormat int

const (
	Motorola ByteFormat = iota
	Intel
)

var byteFormats = [...]string{
	"Motorola",
	"Intel",
}

type DataType int

const (
	Signed DataType = iota
	Unsigned
	Float
	Double
)

var dataTypes = [...]string{
	"Signed",
	"Unsigned",
	"Float",
	"Double",
}

// SignalAttr defines the attributes of a CAN signal.
type SignalAttr struct {
	Name         string
	BitStartLSB0 uint
	Length       uint
	ByteFmt      ByteFormat
	Type         DataType
	Factor       float64
	Offset       float64
	Min          float64
	Max          float64
	Unit         string
	Multiplexor  bool
	Comment      string
	Receivers    []string
	Enums        EnumList
}

// Signal defines the CAN signal along with calculated values.
type Signal struct {
	Attr     SignalAttr
	Min      float64
	Max      float64
	BitStart uint
	AdjStart uint
	Mask     uint64
	CType    string
	Value    interface{}
	Type     types.BasicKind
}

type SignalList []Signal

func (b ByteFormat) String() string { return byteFormats[b] }

func (d DataType) String() string { return dataTypes[d] }

func NewSignal(a SignalAttr) Signal {
	var sig Signal

	sig.Attr = a

	// Use the calculated min/max since it will be used for type determination
	switch a.Type {
	case Unsigned:
		sig.Max = ((math.Pow(2, float64(a.Length)) - 1) * a.Factor) + a.Offset
		sig.Min = a.Offset
		break
	case Signed:
		sig.Max = ((math.Pow(2, (float64(a.Length)-1)) - 1) * a.Factor) + a.Offset
		sig.Min = -(math.Pow(2, (float64(a.Length)-1)) * a.Factor) + a.Offset
		break
	case Float:
		sig.Max = math.MaxFloat32
		sig.Min = -math.MaxFloat32
		break
	case Double:
		sig.Max = math.MaxFloat64
		sig.Min = -math.MaxFloat64
		break
	}

	// Pre-calculate some values
	if a.ByteFmt == Motorola {
		// Custom calculation includes byte swapping
		sig.AdjStart = (63 - ((((a.BitStartLSB0 / 8) * 16) + 7) - a.BitStartLSB0)) - a.Length + 1
		// Convert LSB0 to MSB0
		sig.BitStart = a.BitStartLSB0 - (a.BitStartLSB0 % 8) + 7 - (a.BitStartLSB0 % 8)
	} else {
		sig.AdjStart = a.BitStartLSB0
		sig.BitStart = a.BitStartLSB0
	}

	sig.Mask = ((uint64(0x0000000000000001) << a.Length) - 1)

	if a.Type == Float {
		sig.Value = float32(0.0)
		sig.Type = types.Float32
	} else if a.Type == Double {
		sig.Value = float64(0.0)
		sig.Type = types.Float64
	} else {
		if a.Factor == float64(int64(a.Factor)) {
			if sig.Min < 0 {
				if sig.Max <= math.MaxInt8 && sig.Min >= math.MinInt8 {
					sig.Value = int8(0)
					sig.Type = types.Int8
				} else if sig.Max <= math.MaxInt16 && sig.Min >= math.MinInt16 {
					sig.Value = int16(0)
					sig.Type = types.Int16
				} else if sig.Max <= math.MaxInt32 && sig.Min >= math.MinInt32 {
					sig.Value = int32(0)
					sig.Type = types.Int32
				} else if sig.Max <= math.MaxInt64 && sig.Min >= math.MinInt64 {
					sig.Value = int64(0)
					sig.Type = types.Int64
				}
			} else {
				if sig.Max <= math.MaxUint8 {
					sig.Value = uint8(0)
					sig.Type = types.Uint8
				} else if sig.Max <= math.MaxUint16 {
					sig.Value = uint16(0)
					sig.Type = types.Uint16
				} else if sig.Max <= math.MaxUint32 {
					sig.Value = uint32(0)
					sig.Type = types.Uint32
				} else if sig.Max <= math.MaxUint64 {
					sig.Value = uint64(0)
					sig.Type = types.Uint64
				}
			}
		} else {
			sig.Value = float32(0.0)
		}
	}

	// For backwards compatibility. Change dbtool and remove CType member.
	sig.CType = sig.TypeOfC()

	return sig
}

func (s Signal) String() string {
	return "[" + s.Attr.Name +
		" " + strconv.FormatUint(uint64(s.Attr.BitStartLSB0), 10) +
		" " + strconv.FormatUint(uint64(s.Attr.Length), 10) +
		" " + s.Attr.ByteFmt.String() +
		" " + strconv.FormatFloat(s.Attr.Factor, 'f', -1, 64) +
		" " + strconv.FormatFloat(s.Attr.Offset, 'f', -1, 64) +
		" " + strings.Join(s.Attr.Receivers, " ") + "]"
}

func (s Signal) TypeOf() string {
	return fmt.Sprintf("%T", s.Value)
}

func (s Signal) TypeOfC() string {
	switch s.Value.(type) {
	case float32:
		return "float"
	case float64:
		return "double"
	case int8:
		return "int8_t"
	case int16:
		return "int16_t"
	case int32:
		return "int32_t"
	case int64:
		return "int64_t"
	case uint8:
		return "uint8_t"
	case uint16:
		return "uint16_t"
	case uint32:
		return "uint32_t"
	case uint64:
		return "uint64_t"
	default:
		return fmt.Sprintf("%T", s.Value)
	}
}

func (s Signal) Decode(b []byte) {
	raw := ByteSliceToUint64(b, s)
	raw = (raw >> s.AdjStart) & s.Mask

	switch s.Attr.Type {
	case Unsigned:
		s.scaleUint64(uint64(raw))
		break
	case Signed:
		mask := uint64(2 ^ (s.Attr.Length - 1))
		temp := int64(raw & ^mask) - int64(raw&mask)
		s.scaleInt64(temp)
		break
	case Float:
		s.Value = math.Float32frombits(uint32(raw))
		break
	case Double:
		s.Value = math.Float64frombits(raw)
		break
	}
}

func (s Signal) scaleUint64(v uint64) {
	switch s.Value.(type) {
	case float32:
		s.Value = float32((float32(v) * float32(s.Attr.Factor)) + float32(s.Attr.Offset))
	case float64:
		s.Value = float64((float64(v) * float64(s.Attr.Factor)) + float64(s.Attr.Offset))
	case int8:
		s.Value = int8((int8(v) * int8(s.Attr.Factor)) + int8(s.Attr.Offset))
	case int16:
		s.Value = int16((int16(v) * int16(s.Attr.Factor)) + int16(s.Attr.Offset))
	case int32:
		s.Value = int32((int32(v) * int32(s.Attr.Factor)) + int32(s.Attr.Offset))
	case int64:
		s.Value = int64((int64(v) * int64(s.Attr.Factor)) + int64(s.Attr.Offset))
	case uint8:
		s.Value = uint8((uint8(v) * uint8(s.Attr.Factor)) + uint8(s.Attr.Offset))
	case uint16:
		s.Value = uint16((uint16(v) * uint16(s.Attr.Factor)) + uint16(s.Attr.Offset))
	case uint32:
		s.Value = uint32((uint32(v) * uint32(s.Attr.Factor)) + uint32(s.Attr.Offset))
	case uint64:
		s.Value = uint64((uint64(v) * uint64(s.Attr.Factor)) + uint64(s.Attr.Offset))
	}
}

func (s Signal) scaleInt64(v int64) {
	switch s.Value.(type) {
	case float32:
		s.Value = float32((float32(v) * float32(s.Attr.Factor)) + float32(s.Attr.Offset))
	case float64:
		s.Value = float64((float64(v) * float64(s.Attr.Factor)) + float64(s.Attr.Offset))
	case int8:
		s.Value = int8((int8(v) * int8(s.Attr.Factor)) + int8(s.Attr.Offset))
	case int16:
		s.Value = int16((int16(v) * int16(s.Attr.Factor)) + int16(s.Attr.Offset))
	case int32:
		s.Value = int32((int32(v) * int32(s.Attr.Factor)) + int32(s.Attr.Offset))
	case int64:
		s.Value = int64((int64(v) * int64(s.Attr.Factor)) + int64(s.Attr.Offset))
	case uint8:
		s.Value = uint8((uint8(v) * uint8(s.Attr.Factor)) + uint8(s.Attr.Offset))
	case uint16:
		s.Value = uint16((uint16(v) * uint16(s.Attr.Factor)) + uint16(s.Attr.Offset))
	case uint32:
		s.Value = uint32((uint32(v) * uint32(s.Attr.Factor)) + uint32(s.Attr.Offset))
	case uint64:
		s.Value = uint64((uint64(v) * uint64(s.Attr.Factor)) + uint64(s.Attr.Offset))
	}
}

func (s Signal) scaleFloat32(v float32) {
	switch s.Value.(type) {
	case float32:
		s.Value = float32((float32(v) * float32(s.Attr.Factor)) + float32(s.Attr.Offset))
	case float64:
		s.Value = float64((float64(v) * float64(s.Attr.Factor)) + float64(s.Attr.Offset))
	case int8:
		s.Value = int8((int8(v) * int8(s.Attr.Factor)) + int8(s.Attr.Offset))
	case int16:
		s.Value = int16((int16(v) * int16(s.Attr.Factor)) + int16(s.Attr.Offset))
	case int32:
		s.Value = int32((int32(v) * int32(s.Attr.Factor)) + int32(s.Attr.Offset))
	case int64:
		s.Value = int64((int64(v) * int64(s.Attr.Factor)) + int64(s.Attr.Offset))
	case uint8:
		s.Value = uint8((uint8(v) * uint8(s.Attr.Factor)) + uint8(s.Attr.Offset))
	case uint16:
		s.Value = uint16((uint16(v) * uint16(s.Attr.Factor)) + uint16(s.Attr.Offset))
	case uint32:
		s.Value = uint32((uint32(v) * uint32(s.Attr.Factor)) + uint32(s.Attr.Offset))
	case uint64:
		s.Value = uint64((uint64(v) * uint64(s.Attr.Factor)) + uint64(s.Attr.Offset))
	}
}

func (s Signal) scaleFloat64(v float64) {
	switch s.Value.(type) {
	case float32:
		s.Value = float32((float32(v) * float32(s.Attr.Factor)) + float32(s.Attr.Offset))
	case float64:
		s.Value = float64((float64(v) * float64(s.Attr.Factor)) + float64(s.Attr.Offset))
	case int8:
		s.Value = int8((int8(v) * int8(s.Attr.Factor)) + int8(s.Attr.Offset))
	case int16:
		s.Value = int16((int16(v) * int16(s.Attr.Factor)) + int16(s.Attr.Offset))
	case int32:
		s.Value = int32((int32(v) * int32(s.Attr.Factor)) + int32(s.Attr.Offset))
	case int64:
		s.Value = int64((int64(v) * int64(s.Attr.Factor)) + int64(s.Attr.Offset))
	case uint8:
		s.Value = uint8((uint8(v) * uint8(s.Attr.Factor)) + uint8(s.Attr.Offset))
	case uint16:
		s.Value = uint16((uint16(v) * uint16(s.Attr.Factor)) + uint16(s.Attr.Offset))
	case uint32:
		s.Value = uint32((uint32(v) * uint32(s.Attr.Factor)) + uint32(s.Attr.Offset))
	case uint64:
		s.Value = uint64((uint64(v) * uint64(s.Attr.Factor)) + uint64(s.Attr.Offset))
	}
}

func (s SignalList) Len() int { return len(s) }

func (s SignalList) Less(i, j int) bool { return s[i].Attr.BitStartLSB0 < s[j].Attr.BitStartLSB0 }

func (s SignalList) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

func (s SignalList) Sort() { sort.Sort(s) }

func (s SignalList) Search(x Signal) int {
	return sort.Search(len(s), func(i int) bool {
		return s[i].Attr.BitStartLSB0 >= x.Attr.BitStartLSB0
	})
}

func (s SignalList) Insert(x Signal) SignalList {
	i := s.Search(x)
	if i == len(s) {
		return append(s, x)
	}

	if s[i].Attr.BitStartLSB0 == x.Attr.BitStartLSB0 {
		return s
	}

	var temp Signal
	s = append(s, temp)
	copy(s[i+1:], s[i:])
	s[i] = x
	return s
}

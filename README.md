# cdb #

cdb is a golang library for working with CAN databases.

### Dependencies ###

* golang >= 1.5
* github.com/mattn/go-sqlite3

### Features ###

* Create DB objects from SQLite database (alpha)
* Create DB objects from JSON formatted DBC
* Create DB objects from DBC (deprecated - convert DBCs to JSON)
* Decode raw CAN packets using DB objects

### Structure ###

* Network - For processing CAN data with Go program
  * MessageMap - Hash map of Messages
    * MessageAttr
      * SignalList
    * MuxGroupMap - Hash map of MuxGroup
      * SignalList
  
* NetworkSort - For processing CAN databases
  * NodeList - Slice of Nodes
    * MessageList - Slice of MessageSort
      * MessageAttr
        * SignalList
      * MuxGroupList - Slice of MuxGroup
        * SignalList

### To Do ###

* Extend SQLite functionality (multiplexed messages, comments)
* Better sorting
  * Store all data as sorted lists
  * Use hash maps of pointers
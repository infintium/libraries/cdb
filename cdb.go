package cdb

// CDB defines an entire CAN database map.
type CDB struct {
	Nets []Network
}

package cdb

import "sort"

type Node struct {
	Name     string
	Messages MessageList
}

type NodeSort struct {
	Tx      Node
	Rx      []Node
	NetName string
}

type NodeList []NodeSort

func (n NodeList) Len() int { return len(n) }

func (n NodeList) Less(i, j int) bool { return n[i].Tx.Name < n[j].Tx.Name }

func (n NodeList) Swap(i, j int) { n[i], n[j] = n[j], n[i] }

func (n NodeList) Sort() { sort.Sort(n) }

func (n NodeList) Search(x NodeSort) int {
	return sort.Search(len(n), func(i int) bool {
		return n[i].Tx.Name >= x.Tx.Name
	})
}

func (n NodeList) Insert(x NodeSort) NodeList {
	i := n.Search(x)
	if i == len(n) {
		return append(n, x)
	}

	if n[i].Tx.Name == x.Tx.Name {
		return n
	}

	var temp NodeSort
	n = append(n, temp)
	copy(n[i+1:], n[i:])
	n[i] = x
	return n
}

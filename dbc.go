package cdb

import (
	"encoding/csv"
	"errors"
	"io"
	"os"
)

// There are errors that can be returned.
var (
	ErrInvalidMesssage = errors.New("invalid message")
	ErrInvalidSignal   = errors.New("invalid signal")
)

// NewNetworkFromDBC returns a new CDB after parsing a DBC file.
func NewNetworkFromDBC(name string) (*Network, error) {
	var err error
	net := new(Network)
	net.Name = name
	net.Messages = make(MessageMap)

	file, err := os.Open(name)

	if err != nil {
		return net, err
	}

	defer file.Close()

	r := csv.NewReader(file)
	r.Comma = ' '
	r.FieldsPerRecord = -1
	r.TrimLeadingSpace = true

	var record []string
	var msg Message
	var sig Signal
	for {
		record, err = r.Read()
		if err == io.EOF {
			err = nil
			break
		} else if err == nil {
			if record[0] == "BO_" {
				// Parse message
				msg, err = parseMessage(record)
				if err == nil {
					net.Messages[msg.Attr.ID] = msg
				}
			} else if record[0] == "SG_" {
				// Parse signal
				sig, err = parseSignal(record)
				if err == nil {
					//msg = dbc.Messages[msg.ID]
					msg.Signals = msg.Signals.Insert(sig)
					net.Messages[msg.Attr.ID] = msg
				}
			}
		}
	}

	return net, err
}

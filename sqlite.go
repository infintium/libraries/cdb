package cdb

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

const netQuery = `
SELECT Networks.Name
  FROM Networks;`

const msgQuery = `
SELECT Networks.Name AS Network,
       Nodes.Name AS Node,
       Messages.Name,
       Messages.Type,
       Messages.ID,
       MessageDefs.DLC,
	   Messages.CycleTime,
	   Messages.GenCode
  FROM Networks
       JOIN
       Nodes ON Networks.Name = Nodes.Network
	   JOIN
	   Messages ON Nodes.Network = Messages.Net AND
       AND Nodes.Name = Messages.Transmitter
       JOIN
       MessageDefs ON Messages.MessageDef = MessageDefs.Name
WHERE Networks.Name = ? AND
      Nodes.Name = ?;`

const msgQueryAllNodes = `
SELECT Networks.Name AS Network,
       Nodes.Name AS Node,
       Messages.Name,
       Messages.Type,
       Messages.ID,
       MessageDefs.DLC,
	   Messages.CycleTime,
	   Messages.GenCode
  FROM Networks
       JOIN
       Nodes ON Networks.Name = Nodes.Network
	   JOIN
	   Messages ON Nodes.Network = Messages.Net AND
       Nodes.Name = Messages.Transmitter
       JOIN
       MessageDefs ON Messages.MessageDef = MessageDefs.Name
WHERE Networks.Name = ?;`

const sigQuery = `
SELECT Signals.Name,
       Signals.StartBit,
       Signals.Size,
       Signals.ByteFormat,
       Signals.ValueType,
       Signals.Factor,
       Signals.[Offset],
       Signals.Min,
       Signals.Max,
	   Signals.MultiplexerType,
	   Signals.Unit,
	   Signals.Receiver,
	   Signals.Comment
  FROM Networks
       JOIN
       Nodes ON Networks.Name = Nodes.Network
       JOIN
       Messages ON Nodes.Name = Messages.Transmitter
       JOIN
       Signals ON Messages.MessageDef = Signals.MessageDef
 WHERE Networks.Name = ? AND
       Messages.Transmitter = ? AND
       Messages.Name = ?;`

const enumQuery = `
 SELECT EnumerationValues.Name,
       EnumerationValues.Value
  FROM Networks
       JOIN
       Nodes ON Networks.Name = Nodes.Network
       JOIN
       Messages ON Nodes.Name = Messages.Transmitter
       JOIN
       Signals ON Messages.MessageDef = Signals.MessageDef
       JOIN
       EnumerationValues ON Signals.Enumeration = EnumerationValues.Enumeration
 WHERE Networks.Name = ? AND
       Messages.Name = ? AND 
       Signals.Name = ?;`

func NewCDBFromSQLite(filename string, nodeName string) *CDB {
	db, err := sql.Open("sqlite3", filename)
	if err != nil {
		panic(err)
	}

	rows, err := db.Query(netQuery)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	cdb := new(CDB)

	for rows.Next() {
		var netName string

		err = rows.Scan(&netName)
		if err != nil {
			panic(err)
		}

		cdb.Nets = append(cdb.Nets, getNetworkMessages(db, netName, nodeName))
	}

	db.Close()

	return cdb
}

func getNetworkMessages(db *sql.DB, netName string, nodeName string) Network {
	var net Network

	net.Name = netName
	net.Messages = make(MessageMap)

	var msgRows *sql.Rows
	var err error
	if nodeName == "" {
		msgRows, err = db.Query(msgQueryAllNodes, netName)
		if err != nil {
			panic(err)
		}
	} else {
		msgRows, err = db.Query(msgQuery, netName, nodeName)
		if err != nil {
			panic(err)
		}
	}
	defer msgRows.Close()

	for msgRows.Next() {
		var networkName string
		var transmitter string
		var msgName string
		var msgType string
		var id int
		var dlc int
		var cycleTime int
		var genCode bool
		err = msgRows.Scan(&networkName, &transmitter, &msgName, &msgType, &id, &dlc, &cycleTime, &genCode)
		if err != nil {
			panic(err)
		}
		fmt.Println(networkName, transmitter, msgName, id, dlc)

		mattr := MessageAttr{
			ID:             uint32(id),
			Name:           msgName,
			DLC:            uint(dlc),
			Sender:         transmitter,
			HasMultiplexor: false,
			CycleTime:      uint(cycleTime),
			GenCode:        genCode}

		if msgType == "Extended" {
			mattr.ID |= 0x80000000
		}

		msg := Message{Attr: mattr}
		msg.MuxSignals = make(MuxGroupMap)

		sigRows, err := db.Query(sigQuery, netName, transmitter, msgName)
		if err != nil {
			panic(err)
		}
		defer sigRows.Close()

		for sigRows.Next() {
			var signalName string
			var startBit int
			var size int
			var byteFormat string
			var valueType string
			var factor float64
			var offset float64
			var min sql.NullFloat64
			var max sql.NullFloat64
			var multiplexerType string
			var unit sql.NullString
			var receiver sql.NullString
			var comment sql.NullString

			err = sigRows.Scan(&signalName, &startBit, &size, &byteFormat, &valueType,
				&factor, &offset, &min, &max, &multiplexerType, &unit, &receiver, &comment)
			if err != nil {
				panic(err)
			}
			fmt.Println(signalName, startBit, size, byteFormat, valueType,
				factor, offset, min, max, unit, multiplexerType)

			sattr := SignalAttr{
				Name:         signalName,
				BitStartLSB0: uint(startBit),
				Length:       uint(size),
				Type:         Unsigned,
				Factor:       factor,
				Offset:       offset,
				Min:          min.Float64,
				Max:          max.Float64,
				Unit:         unit.String,
				Multiplexor:  multiplexerType == "Multiplexer Signal",
				Comment:      comment.String,
				Receivers:    strings.Split(receiver.String, ",")}
			if byteFormat == "Intel" {
				sattr.ByteFmt = Intel
			} else {
				sattr.ByteFmt = Motorola
			}
			if valueType == "Unsigned" {
				sattr.Type = Unsigned
			} else if valueType == "Signed" {
				sattr.Type = Signed
			} else if valueType == "IEEE Float" {
				sattr.Type = Float
			} else if valueType == "IEEE Double" {
				sattr.Type = Double
			}

			sattr.Enums = getSignalEnums(db, netName, msgName, signalName)

			fmt.Println("  ", sattr.Enums)

			sig := NewSignal(sattr)

			if multiplexerType == "Multiplexer Signal" {
				msg.Attr.Multiplexor = sig
			} else if multiplexerType == "Signal" {
				msg.Signals = msg.Signals.Insert(sig)
			} else {
				mid := uint(0)
				msig, ok := msg.MuxSignals[mid]
				if !ok {
					msig.ID = mid
				}
				msig.Signals = msig.Signals.Insert(sig)
				msg.MuxSignals[mid] = msig

			}
		}

		net.Messages[uint32(id)] = msg
	}

	return net
}

func getSignalEnums(db *sql.DB, netName string, msgName string, sigName string) EnumList {
	var enums EnumList

	rows, err := db.Query(enumQuery, netName, msgName, sigName)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var name string
		var value int

		err = rows.Scan(&name, &value)
		if err != nil {
			panic(err)
		}

		enum := Enumeration{
			Name:  name,
			Value: value}

		enums = enums.Insert(enum)
	}

	return enums
}

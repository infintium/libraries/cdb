package cdb

import "sort"

type Enumeration struct {
	Value int
	Name  string
}

type EnumList []Enumeration

func (e EnumList) Len() int { return len(e) }

func (e EnumList) Less(i, j int) bool { return e[i].Value < e[j].Value }

func (e EnumList) Swap(i, j int) { e[i], e[j] = e[j], e[i] }

func (e EnumList) Sort() { sort.Sort(e) }

func (e EnumList) Search(x Enumeration) int {
	return sort.Search(len(e), func(i int) bool {
		return e[i].Value >= x.Value
	})
}

func (e EnumList) Insert(x Enumeration) EnumList {
	i := e.Search(x)
	if i == len(e) {
		return append(e, x)
	}

	if e[i].Value == x.Value {
		return e
	}

	var temp Enumeration
	e = append(e, temp)
	copy(e[i+1:], e[i:])
	e[i] = x
	return e
}

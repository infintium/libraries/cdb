package cdb

import (
	"sort"
	"strconv"
)

// Message defines the encoding of a CAN message.
type MessageAttr struct {
	ID             uint32
	Name           string
	Sender         string
	DLC            uint
	HasMultiplexor bool
	CycleTime      uint
	Multiplexor    Signal
	GenCode        bool
}

type Message struct {
	Attr       MessageAttr
	MuxSignals MuxGroupMap
	Signals    SignalList
}

type MessageSort struct {
	Attr       MessageAttr
	MuxSignals MuxGroupList
	Signals    SignalList
}

type MessageMap map[uint32]Message
type MessageList []MessageSort

func (m Message) String() string {
	return "[" + m.Attr.Name +
		" 0x" + strconv.FormatUint(uint64(m.Attr.ID), 16) +
		" " + strconv.FormatUint(uint64(m.Attr.DLC), 10) + "]"
}

func (m MessageSort) String() string {
	return "[" + m.Attr.Name +
		" 0x" + strconv.FormatUint(uint64(m.Attr.ID), 16) +
		" " + strconv.FormatUint(uint64(m.Attr.DLC), 10) + "]"
}

func (m MessageList) Len() int { return len(m) }

func (m MessageList) Less(i, j int) bool { return m[i].Attr.ID < m[j].Attr.ID }

func (m MessageList) Swap(i, j int) { m[i], m[j] = m[j], m[i] }

func (m MessageList) Sort() { sort.Sort(m) }

func (m MessageList) Search(x MessageSort) int {
	return sort.Search(len(m), func(i int) bool {
		return m[i].Attr.ID >= x.Attr.ID
	})
}

func (m MessageList) Insert(x MessageSort) MessageList {
	i := m.Search(x)
	if i == len(m) {
		return append(m, x)
	}

	if m[i].Attr.ID == x.Attr.ID {
		return m
	}

	var temp MessageSort
	m = append(m, temp)
	copy(m[i+1:], m[i:])
	m[i] = x
	return m
}

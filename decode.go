package cdb

import (
	"encoding/binary"
	"math"
	"strconv"
)

// FormatSignal generates a string of the signal value
func FormatSignal(data []byte, sig Signal) string {
	temp := ByteSliceToUint64(data, sig)
	temp = ShiftSignal(temp, sig)

	if sig.CType == "float" {
		value := ScaleUSignalFloat(temp, sig)
		return strconv.FormatFloat(value, 'f', 6, 64)
	}

	if sig.Min < 0 {
		value := ScaleUSignalInt(temp, sig)
		return strconv.FormatInt(value, 10)
	}

	value := ((temp * uint64(sig.Attr.Factor)) + uint64(sig.Attr.Offset))
	if sig.Attr.Length == 1 {
		b := bool(value != 0)
		return strconv.FormatBool(b)
	}

	return strconv.FormatUint(value, 10)
}

func InterfaceSignal(data []byte, sig Signal) interface{} {
	raw := ByteSliceToUint64(data, sig)
	// fmt.Println(sig.Attr.Name, sig.Attr.ByteFmt, strconv.FormatUint(raw, 16))
	raw = ShiftSignal(raw, sig)
	// fmt.Println(sig.Attr.Name, sig.BitStart, strconv.FormatUint(raw, 16))

	switch sig.Attr.Type {
	case Unsigned:
		if sig.CType == "float" {
			return ScaleUSignalFloat(raw, sig)
		}
		return ScaleUSignalInt(raw, sig)
	case Signed:
		mask := uint64(1 << (sig.Attr.Length - 1))
		// fmt.Println(strconv.FormatUint(mask, 16))
		temp := int64(raw & ^mask) - int64(raw&mask)
		// fmt.Println(strconv.FormatUint(mask, 16), temp)
		if sig.CType == "float" {
			return ScaleSignalFloat(temp, sig)
		}
		return ScaleSignalInt(temp, sig)
	case Float:
		return math.Float32frombits(uint32(raw))
	case Double:
		return math.Float64frombits(raw)
	}

	// if sig.CType == "float" {
	// 	return ScaleSignalFloat(temp, sig)
	// }

	// if sig.Attr.Length == 1 {
	// 	return bool(((temp * uint64(sig.Attr.Factor)) + uint64(sig.Attr.Offset)) != 0)
	// }

	// return ScaleSignalInt(temp, sig)
	return raw
}

func ByteSliceToUint64(data []byte, sig Signal) uint64 {
	if sig.Attr.ByteFmt == Motorola {
		return binary.BigEndian.Uint64(data)
	}
	return binary.LittleEndian.Uint64(data)
}

func ShiftSignal(data uint64, sig Signal) uint64 {
	if sig.Attr.ByteFmt == Motorola {
		return (data >> (64 - (sig.BitStart + sig.Attr.Length))) & sig.Mask
	}
	return (data >> sig.BitStart) & sig.Mask
}

func ScaleUSignalFloat(data uint64, sig Signal) float64 {
	return ((float64(data) * sig.Attr.Factor) + sig.Attr.Offset)
}

func ScaleSignalFloat(data int64, sig Signal) float64 {
	return ((float64(data) * sig.Attr.Factor) + sig.Attr.Offset)
}

func ScaleUSignalInt(data uint64, sig Signal) int64 {
	return ((int64(data) * int64(sig.Attr.Factor)) + int64(sig.Attr.Offset))
}

func ScaleSignalInt(data int64, sig Signal) int64 {
	return ((int64(data) * int64(sig.Attr.Factor)) + int64(sig.Attr.Offset))
}

func ScaleSignalBool(data uint64, sig Signal) bool {
	value := ((data * uint64(sig.Attr.Factor)) + uint64(sig.Attr.Offset))
	return bool(value != 0)
}

// ParseSignal - convert signal to raw data
// func ParseSignal(sig Signal) []byte

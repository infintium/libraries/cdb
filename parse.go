package cdb

import (
	"math"
	"regexp"
	"strconv"
	"strings"
)

func parseMessage(s []string) (msg Message, err error) {
	var temp uint64

	if s[0] != "BO_" {
		return msg, err
	}

	temp, err = strconv.ParseUint(s[1], 10, 32)
	if err != nil {
		return msg, err
	}
	msg.Attr.ID = uint32(temp) & 0x3FFFFFFF

	msg.Attr.Name = strings.TrimSuffix(s[2], ":")

	temp, err = strconv.ParseUint(s[3], 10, 8)
	if err != nil {
		return msg, err
	}
	msg.Attr.DLC = uint(temp)

	return msg, err
}

func parseSignal(s []string) (sig Signal, err error) {
	var temp uint64

	if s[0] != "SG_" {
		return sig, err
	}

	sig.Attr.Name = s[1]

	// Parse the start, length and type substring
	fields := regexp.MustCompile("[|@]").Split(s[3], 3)

	temp, err = strconv.ParseUint(fields[0], 10, 8)
	if err != nil {
		return sig, err
	}
	sig.Attr.BitStartLSB0 = uint(temp)

	temp, err = strconv.ParseUint(fields[1], 10, 8)
	if err != nil {
		return sig, err
	}
	sig.Attr.Length = uint(temp)

	if fields[2][0] == '0' {
		sig.Attr.ByteFmt = Motorola
	} else if fields[2][0] == '1' {
		sig.Attr.ByteFmt = Intel
	} else {
		return sig, error(ErrInvalidSignal)
	}

	if fields[2][1] == '+' {
		sig.Attr.Type = Unsigned
	} else if fields[2][1] == '-' {
		sig.Attr.Type = Signed
	} else {
		return sig, error(ErrInvalidSignal)
	}

	// Parse the factor and offset substring
	fields = regexp.MustCompile(`[\(,\)]`).Split(s[4], 4)

	tempFactor, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		sig.Attr.Factor, err = strconv.ParseFloat(fields[1], 64)
		if err != nil {
			return sig, err
		}

		//sig.IsInt = false
	} else {
		sig.Attr.Factor = float64(tempFactor)
		//sig.IsInt = true
	}

	sig.Attr.Offset, err = strconv.ParseFloat(fields[2], 64)
	if err != nil {
		return sig, err
	}

	// Use the calculated min/max since it will be used for type determination
	switch sig.Attr.Type {
	case Unsigned:
		sig.Max = ((math.Pow(2, float64(sig.Attr.Length)) - 1) * sig.Attr.Factor) + sig.Attr.Offset
		sig.Min = sig.Attr.Offset
		break
	case Signed:
		sig.Max = ((math.Pow(2, (float64(sig.Attr.Length)-1)) - 1) * sig.Attr.Factor) + sig.Attr.Offset
		sig.Min = -(math.Pow(2, (float64(sig.Attr.Length)-1)) * sig.Attr.Factor) + sig.Attr.Offset
		break
	case Float:
		sig.Max = math.MaxFloat32
		sig.Min = -math.MaxFloat32
		break
	case Double:
		sig.Max = math.MaxFloat64
		sig.Min = -math.MaxFloat64
		break
	}

	// Parse the min and max substring
	fields = regexp.MustCompile(`[\[|\]]`).Split(s[5], 4)

	sig.Attr.Min, err = strconv.ParseFloat(fields[1], 64)
	if err != nil {
		return sig, err
	}

	sig.Attr.Max, err = strconv.ParseFloat(fields[2], 64)
	if err != nil {
		return sig, err
	}

	//if max != sig.Max {
	//	fmt.Println("Calculated max does not equal DBC max")
	//}

	//if min != sig.Min {
	//	fmt.Println("Calculated min does not equal DBC min")
	//}

	// Pre-calculate some values
	if sig.Attr.ByteFmt == Motorola {
		sig.AdjStart = (63 - ((((sig.Attr.BitStartLSB0 / 8) * 16) + 7) - sig.Attr.BitStartLSB0)) - sig.Attr.Length + 1
	} else {
		sig.AdjStart = sig.Attr.BitStartLSB0
	}

	sig.Mask = ((uint64(0x0000000000000001) << sig.Attr.Length) - 1)

	return sig, err
}

package cdb

import "sort"

type MuxGroup struct {
	ID      uint
	Signals SignalList
}

type MuxGroupMap map[uint]MuxGroup
type MuxGroupList []MuxGroup

func (m MuxGroupList) Len() int { return len(m) }

func (m MuxGroupList) Less(i, j int) bool { return m[i].ID < m[j].ID }

func (m MuxGroupList) Swap(i, j int) { m[i], m[j] = m[j], m[i] }

func (m MuxGroupList) Sort() { sort.Sort(m) }

func (m MuxGroupList) Search(x MuxGroup) int {
	return sort.Search(len(m), func(i int) bool {
		return m[i].ID >= x.ID
	})
}

func (m MuxGroupList) Insert(x MuxGroup) MuxGroupList {
	i := m.Search(x)
	if i == len(m) {
		return append(m, x)
	}

	if m[i].ID == x.ID {
		return m
	}

	var temp MuxGroup
	m = append(m, temp)
	copy(m[i+1:], m[i:])
	m[i] = x
	return m
}

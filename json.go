package cdb

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type JSONSigAttr struct {
	Name string `json:"name"`
	// BitStart     uint    `json:"bit_start"`
	BitStart uint `json:"start_bit"`
	// Length   uint `json:"length"`
	Length uint `json:"bit_length"`
	// LittleEndian uint    `json:"little_endian"`
	IsBigEndian bool `json:"is_big_endian"`
	// Signed       uint    `json:signed`
	Signed       bool    `json:"is_signed"`
	Factor       float64 `json:"factor"`
	Offset       float64 `json:"offset"`
	Min          float64 `json:"min"`
	Max          float64 `json:"max"`
	Unit         string  `json:"unit"`
	Multiplexing *uint   `json:"multiplexing"`
	Multiplexor  bool    `json:"multiplexor"`
}

type JSONSig []JSONSigAttr

type JSONMsgAttr struct {
	ID             int     `json:"id"`
	IsExtended     bool    `json:"is_extended_frame"`
	Name           string  `json:"name"`
	Sender         string  `json:"sender"`
	DLC            uint    `json:"length"`
	Signals        JSONSig `json:"signals"`
	HasMultiplexor bool    `json:"has_multiplexor"`
}

type JSONMsg []JSONMsgAttr

type JSONDBC struct {
	// Filename string  `json:"filename"`
	Messages JSONMsg `json:"messages"`
}

func NewNetworkFromJSON(filename string, name string) *Network {
	file, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	//fmt.Printf("%s\n", string(file))

	var jsondbc JSONDBC
	err := json.Unmarshal(file, &jsondbc)
	if err != nil {
		fmt.Println(err)
	}

	net := new(Network)
	net.Name = name
	net.Messages = make(MessageMap)

	for _, jmsg := range jsondbc.Messages {
		// id64, _ := strconv.ParseUint(idStr, 10, 32)
		id := uint32(jmsg.ID)
		if jmsg.IsExtended == true {
			id = id | 0x80000000
		}

		mattr := MessageAttr{
			ID:             id,
			Name:           jmsg.Name,
			DLC:            jmsg.DLC,
			Sender:         jmsg.Sender,
			HasMultiplexor: jmsg.HasMultiplexor}
		msg := Message{Attr: mattr}
		msg.MuxSignals = make(MuxGroupMap)

		for _, jsig := range jmsg.Signals {
			sattr := SignalAttr{
				Name:         jsig.Name,
				BitStartLSB0: jsig.BitStart,
				Length:       jsig.Length,
				Type:         Unsigned,
				Factor:       jsig.Factor,
				Offset:       jsig.Offset,
				Min:          jsig.Min,
				Max:          jsig.Max,
				Unit:         jsig.Unit,
				Multiplexor:  jsig.Multiplexor}
			if jsig.IsBigEndian == true {
				sattr.ByteFmt = Motorola
			} else {
				sattr.ByteFmt = Intel
			}
			if jsig.Signed == false {
				sattr.Type = Unsigned
			} else {
				sattr.Type = Signed
			}
			sig := NewSignal(sattr)

			if jsig.Multiplexor == true {
				msg.Attr.Multiplexor = sig
			} else if jsig.Multiplexing == nil {
				msg.Signals = msg.Signals.Insert(sig)
			} else {
				mid := *jsig.Multiplexing
				msig, ok := msg.MuxSignals[mid]
				if !ok {
					msig.ID = mid
				}
				msig.Signals = msig.Signals.Insert(sig)
				msg.MuxSignals[mid] = msig

			}
		}

		net.Messages[id] = msg
	}

	return net
}

package cdb

import "fmt"

// Network uses hash maps to quickly lookup messages.
type Network struct {
	Name     string
	Messages MessageMap
}

// NetworkSort uses slices for sorted processing.
type NetworkSort struct {
	Name     string
	Nodes    NodeList
	Messages MessageList
}

// PrintNetwork will print the CDB object.
func (n *Network) PrintNetwork() {
	for _, m := range n.Messages {
		fmt.Printf("%s %x %d %d\n", m.Attr.Name, m.Attr.ID, m.Attr.DLC, len(m.Signals))
		for _, s := range m.Signals {
			fmt.Printf("\t%s %d %d %t %t %f %f\n", s.Attr.Name, s.Attr.BitStartLSB0, s.Attr.Length, s.Attr.Type, s.Attr.ByteFmt, s.Attr.Factor, s.Attr.Offset)
		}
	}
}

// PrintNetwork will print the CDB object.
func (n *NetworkSort) PrintNetworkSort() {
	for _, nd := range n.Nodes {
		fmt.Println("Node: ", nd.Tx.Name)
		fmt.Println("Tx Messages:")
		for _, m := range nd.Tx.Messages {
			fmt.Println("  ", m)
			for _, s := range m.Signals {
				fmt.Println("    ", s)
			}
		}
		fmt.Println("Rx Messages:")
		for _, n := range nd.Rx {
			for _, m := range n.Messages {
				fmt.Println("  ", m)
				for _, s := range m.Signals {
					fmt.Println("    ", s)
				}
			}
		}
	}
}

func (n *Network) Sort() *NetworkSort {
	s := new(NetworkSort)

	s.Name = n.Name

	// Create a temporary map of sorted nodes
	var nsm map[string]NodeSort
	nsm = make(map[string]NodeSort)

	// Create a temporary map of nodes for Rx
	var nm map[string]Node
	nm = make(map[string]Node)

	for _, m := range n.Messages {

		// Sort Mux signals into a list
		var mgl MuxGroupList
		for _, x := range m.MuxSignals {
			mgl = mgl.Insert(x)
		}

		var ms = MessageSort{Attr: m.Attr, MuxSignals: mgl, Signals: m.Signals}

		// Get node and insert message
		var ns = nsm[m.Attr.Sender]
		ns.Tx.Name = m.Attr.Sender
		ns.NetName = s.Name
		ns.Tx.Messages = ns.Tx.Messages.Insert(ms)
		nsm[m.Attr.Sender] = ns

		s.Messages = s.Messages.Insert(ms)

		// Create temporary map of nodes -> messages
		var recMap map[string]MessageSort
		recMap = make(map[string]MessageSort)

		// Loop through signals to get receivers
		for _, sig := range m.Signals {
			for _, rec := range sig.Attr.Receivers {
				var msg = recMap[rec]
				msg.Attr = m.Attr
				msg.Signals = msg.Signals.Insert(sig)
				recMap[rec] = msg
			}
		}

		// Loop through message map and add to node map
		for recName, recMsg := range recMap {
			var n = nm[recName]
			n.Name = recName
			n.Messages = n.Messages.Insert(recMsg)
			nm[recName] = n
		}
	}

	for name, n := range nm {
		// Create a temporary map of nodes for Rx
		var rnm map[string]Node
		rnm = make(map[string]Node)

		for _, m := range n.Messages {
			var nd = rnm[m.Attr.Sender]
			nd.Name = m.Attr.Sender
			nd.Messages = nd.Messages.Insert(m)
			rnm[m.Attr.Sender] = nd
		}

		for _, nd := range rnm {
			var ns = nsm[name]
			ns.Tx.Name = name
			ns.NetName = s.Name
			ns.Rx = append(ns.Rx, nd)
			nsm[name] = ns
		}
	}

	for _, nd := range nsm {
		s.Nodes = s.Nodes.Insert(nd)
	}

	return s
}
